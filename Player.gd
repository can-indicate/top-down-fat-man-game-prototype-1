extends KinematicBody2D


onready var Projectile = preload("res://FriendlyProjectile.tscn")
onready var Blood = preload("res://BloodSprite.tscn")
var level
var facing_side
var stopped
var hit_circle
var r0
var strafe = false

func _ready():
	level = get_parent().get_parent()
	r0 = $CollisionShape2D.shape.radius
	hit_circle = $CollisionShape2D

var velocity = Vector2()
var speed = 200.0
var health = 100.0
var powerup = false
var touch_events = []

func add_touch_event(event):
	touch_events.append(event)

func take_damage(damage, positon):
	health -= damage
	speed += damage
	var size = max(health/100.0, 0.75)
	$CollisionShape2D.shape.radius = r0*size
	var t = $Sprite.get_canvas_transform()
	t.x = Vector2(size, 0.0)
	t.y = Vector2(0.0, size)
	t.origin = Vector2(0.0, 0.0)
	$Sprite.set_transform(t)
	var blood = Blood.instance()
	blood.global_position = position
	level.add_sprite(blood)
	if health <= 0:
		level.set_ui_position(global_position)
		level.show_game_over_display()
		queue_free()
	handle_health_display()
	
func _health():
	var size = max(self.health/100.0, 0.75)
	$CollisionShape2D.shape.radius = r0*size
	var t = $Sprite.get_canvas_transform()
	print(t)
	t.x = Vector2(size, 0.0)
	t.y = Vector2(0.0, size)
	t.origin = Vector2(0.0, 0.0)
	$Sprite.set_transform(t)
	handle_health_display()

func add_health(health):
	self.health += health
	self.speed -= health
	_health()
	
func set_health(health):
	self.health = health
	self.speed = 300.0 - health
	_health()

func handle_health_display():
	level.update_health_display(health)
	
func use_powerup():
	if get_node("PowerUpTimer").time_left == 0:
		powerup = true
		get_node("PowerUpTimer").start(10)
		
func fire_single_shot(dir):
	var projectile_speed = 200.0
	if strafe:
		look_at(get_global_mouse_position())
	if get_node("FireTimer").time_left == 0:
		var p = Projectile.instance()
		p.velocity = dir*projectile_speed*2.0
		p.set_global_position(get_node("Position2D").global_position)
		level.add_object(p)
		get_node("FireTimer").start(0.1)
		
func fire_multiple_shots(dir):
	var projectile_speed = 200.0
	if strafe:
		look_at(get_global_mouse_position())
	if get_node("FireTimer").time_left == 0:
		var pc = Projectile.instance()
		var pl = Projectile.instance()
		var pr = Projectile.instance()
		var pl2 = Projectile.instance()
		var pr2 = Projectile.instance()
		pc.velocity = dir*projectile_speed*2.5
		pl.velocity = dir.rotated(-0.25)*projectile_speed*2.5
		pr.velocity = dir.rotated(0.25)*projectile_speed*2.5
		pl2.velocity = dir.rotated(-0.5)*projectile_speed*2.5
		pr2.velocity = dir.rotated(0.5)*projectile_speed*2.5
		var projectiles = [pc, pl, pr, pl2, pr2]
		for p in projectiles:
			p.set_global_position(get_node("Position2D").global_position)
		level.add_objects(projectiles)
		get_node("FireTimer").start(0.075)

func _physics_process(delta):
	# look_at(get_global_mouse_position())
	var dir = -(global_position - get_global_mouse_position()).normalized()
	var move = Vector2(0.0, 0.0)
	if len(touch_events) > 0:
		var event = touch_events[0]
		move = (position - global_position).normalized()
		touch_events = []
	if Input.is_action_just_released("q"):
		strafe = not strafe
	if Input.is_action_pressed("right"):
		move.x = 1.0
	if Input.is_action_pressed("left"):
		move.x = -1.0
	if Input.is_action_pressed("up"):
		move.y = -1.0
	if Input.is_action_pressed("down"):
		move.y = 1.0
	move = move.normalized()
	look_at(global_position + move)
	if (Input.is_action_pressed("left_click") or 
		Input.is_action_pressed("attack")):
		var not_strafe_dir = Vector2(1.0, 0.0).rotated(
			get_global_transform().get_rotation()).normalized()
		if powerup:
			fire_multiple_shots(dir if strafe else not_strafe_dir)
		else:
			fire_single_shot(dir if strafe else not_strafe_dir)
	velocity = move_and_slide(speed*move)
	level.set_ui_position(global_position)

func _on_PowerUpTimer_timeout():
	powerup = false
