# References and Resources #

*  [TopDownShooter](https://github.com/Miziziziz/TopDownShooter) by Miziziziz
*  [2D Navigation Demo](https://godotengine.org/asset-library/asset/117) (Godot Engine demo)
*  [godot-pathfinding2d-demo](https://github.com/FEDE0D/godot-pathfinding2d-demo) by FEDE0D

## Art Assets ##
*  [Top down shooter assets](https://www.kenney.nl/assets/topdown-shooter) by Kenney
*  [Bullet Collection 1](https://opengameart.org/content/bullet-collection-1-m484) by Master484
*  [Explosion assets](https://opengameart.org/content/explosion) by Cuzco
*  [Blood Effects](https://xyezawr.itch.io/gif-free-pixel-effects-pack-5-blood-effects) by XYEzawr1
*  [Free Pixel foods](https://ghostpixxells.itch.io/pixelfood) by ghostpixxells
*  Map and 'Java' image textures are created with GIMP
*  [Doors](https://opengameart.org/content/doors) by SpiderDave

## Sound Assets ##
*  [Bomb](https://soundbible.com/1234-Bomb.html) by Mike Koenig ([CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)). Sound effect sped up.
*  [Gun Fire](https://soundbible.com/1998-Gun-Fire.html) by GoodSoundForYou ([CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)). Later portions cut out.
*  [Squish 1](https://soundbible.com/511-Squish-1.html), [Squish 2](https://soundbible.com/512-Squishy-2.html), [Blood Splatters](https://soundbible.com/824-Blood-Splatters.html), [Blood Squirt](https://soundbible.com/828-Blood-Squirt.html), and [Spit Splat](https://soundbible.com/1733-Spit-Splat.html) by Mike Koeing ([CC BY 3.0](https://creativecommons.org/licenses/by/3.0/))

