extends Node2D

onready var Enemy = preload("res://Enemy.tscn")
onready var EnemyWithGun = preload("res://EnemyWithGun.tscn")
onready var Hamburger = preload("res://Hamburger.tscn")
onready var FrenchFries = preload("res://FrenchFries.tscn")
var player
var points
var health_display
var game_over_display
var objects
var ui
var pause
var navigation_map
var disable_enemy_spawn = false

func _ready(): 
	player = $Entities/Player
	points = $UIDisplay/PointsDisplay
	health_display = $UIDisplay/HealthDisplay
	game_over_display = $UIDisplay/GameOverDisplay
	ui = $UIDisplay
	pause = $PauseDisplay
	objects = $Objects
	if $Map.get_child_count() > 0:
		print($Map.get_children()[0])
		navigation_map = $Map.get_children()[0]
	else:
		navigation_map = null
	load_game()
		
func go_to(destination):
	var dest = load('res://' + destination + '.tscn')
	save_game({'health': player.health})
	queue_free()
	var dest_instance = dest.instance()
	get_tree().get_root().add_child(dest_instance)

func get_navigation_map():
	return navigation_map
	
func set_ui_position(position):
	var t = get_viewport_transform()
	t.origin = -position + get_viewport_rect().size/2.0
	get_viewport().set_canvas_transform(t)
	ui.global_position = position  - get_viewport_rect().size/2.0
	pause.global_position = position

func add_sound(sound):
	objects.add_child(sound)

func add_sprite(sprite):
	objects.add_child(sprite)

func add_object(obj):
	objects.add_child(obj)
	
func add_item(item):
	objects.add_child(item)
	
func add_objects(obj_list):
	for o in obj_list:
		objects.add_child(o)

func add_points(n_points):
	points.add_points(n_points)
	
func update_health_display(health):
	health_display.update_health(health)
	
func show_game_over_display():
	game_over_display.visible = true
	save_game({'health': 100.0})
	
func create_random_enemy(rng):
	rng.randomize()
	var enemy
	if rng.randf() < 0.75:
		enemy = Enemy.instance()
	else:
		enemy = EnemyWithGun.instance()
	enemy.global_position = get_random_position(rng)
	return enemy
	
func add_enemy(enemy):
	if not disable_enemy_spawn:
		$Entities.add_child(enemy)
	
func get_player():
	return $Entities/Player

func get_random_position(rng):
	var h = 600
	var w = 1000
	rng.randomize()
	var x = w*rng.randf()
	rng.randomize()
	var y = h*rng.randf()
	if get_navigation_map() != null:
		var map = get_navigation_map()
		var position = Vector2(x, y)
		var position2 = map.get_closest_point(position)
		if position.distance_to(position2) > 0:
			return get_random_position(rng)
		else:
			return position 
	return Vector2(x, y)

func _on_EnemySpawnTimer_timeout():
	var rng = RandomNumberGenerator.new()
	var enemy = create_random_enemy(rng)
	if is_instance_valid(player) and player.health > 0:
		add_enemy(enemy)


func _on_FoodSpawnTimer_timeout():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var food = Hamburger.instance()
	if rng.randf() < 0.5:
		food = FrenchFries.instance()
	food.global_position = get_random_position(rng)
	if is_instance_valid(player) and player.health > 0:
		add_object(food)
		
func load_game():
	var load_file = File.new()
	var points = 0
	var health = 100
	if load_file.file_exists("res://game.save"):
		load_file.open("res://game.save", File.READ)
		var data = JSON.parse(load_file.get_line()).get_result()
		if data != null:
			if data.has('score'):
				points = data['score']
			if data.has('health'):
				health = data['health']
				player.set_health(health)
	$UIDisplay/PointsDisplay.set_high_score(points)
	
		
func save_game(player_stats):
	var load_file = File.new()
	var points = 0
	if load_file.file_exists("res://game.save"):
		load_file.open("res://game.save", File.READ)
		var data = JSON.parse(load_file.get_line()).get_result()
		if data != null:
			if data.has('score'):
				points = data['score']
	var save_file = File.new()
	save_file.open("res://game.save", File.WRITE)
	var new_points = $UIDisplay/PointsDisplay.points
	var max_points = new_points if new_points > points else points
	save_file.store_line(to_json({"score": max_points,
								  "health": player_stats['health'],
								  "level": filename}))


func _unhandled_input(event):
	# Maybe this is helpful
	# https://godotengine.org/qa/68284/
	# how-to-properly-use-inputeventscreentouch-for-mobile
	if event is InputEventScreenTouch:
		if event.is_pressed():
			player.add_touch_event(event)
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_ENTER:
			if not is_instance_valid(player) or player.health < 0:
				save_game({'health': 100.0})
				var MainTitle = load("res://MainTitle.tscn")
				set_ui_position(get_viewport_rect().size/2.0)
				queue_free()
				get_tree().get_root().add_child(MainTitle.instance())
		

func _exit_tree():
	save_game({"health": 100})

