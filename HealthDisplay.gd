extends Node2D

var healthy
var sick
var close_to_death
var dead
var stats

func _ready():
	healthy = get_node("RichTextLabel2")
	sick = get_node("RichTextLabel3")
	close_to_death = get_node("RichTextLabel4")
	dead = get_node("RichTextLabel5")
	stats = [healthy, sick, close_to_death, dead]
	healthy.visible = true
	
func update_health(health):
	var text = get_node("RichTextLabel2")
	for status in stats:
		status.visible = false
	if health > 75:
		healthy.visible = true
	elif health > 25:
		sick.visible = true
	elif health > 0:
		close_to_death.visible = true
	else:
		dead.visible = true


