extends Sprite


var destination = 'Level3'
var level

func _ready():
	level = get_parent()


func _on_Area2D_body_entered(body):
	if body.has_method('add_health'):
		level.go_to(destination)
