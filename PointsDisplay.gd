extends Node2D


var points = 0

func set_high_score(high_score):
	get_node("RichTextLabel2").text = "High Score: " + str(high_score)


func add_points(points):
	self.points += points
	get_node("RichTextLabel").text = "Points: " + str(self.points)

