extends Sprite

var level

func _ready():
	level = get_parent()

func _process(delta):
	if Input.is_action_just_pressed("ui_accept") and $Timer.time_left == 0:
		if level.get_tree().paused == true:
			level.get_tree().paused = false
			visible = false
		else:
			level.get_tree().paused = true
			visible = true


