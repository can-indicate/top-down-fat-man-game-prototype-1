extends KinematicBody2D


var health = 200


var velocity = Vector2(0.0, 0.0)
var level
var player

onready var Projectile = preload("res://EnemyProjectile.tscn")
onready var Explosion = preload("res://ExplosionSprite.tscn")
onready var PowerUp = preload("res://PowerUp.tscn")


func _ready():
	level = get_parent().get_parent()
	player = level.get_player()

func enemy_take_damage(damage, position):
	var explosion = Explosion.instance()
	explosion.global_position = position
	level.add_sprite(explosion)
	health -= damage
	check_health()
	
func check_health():
	if health <= 0:
		queue_free()
		level.add_points(1)
		var explosion = Explosion.instance()
		explosion.global_position = global_position
		level.add_sprite(explosion)
		var rng = RandomNumberGenerator.new()
		rng.randomize()
		if rng.randf() < 0.75:
			if is_instance_valid(player) and player.health > 0:
				if player.get_node("PowerUpTimer").time_left == 0:
					var powerup = PowerUp.instance()
					powerup.global_position = global_position
					level.add_sprite(powerup)
					
func _move_and_shoot():
	if is_instance_valid(player) and player.health > 0:
		if (level.get_navigation_map() != null):
			var nav = level.get_navigation_map()
			var path = nav.get_simple_path(global_position, 
										   player.global_position)
			var dir = Vector2(0.0, 0.0)
			if len(path) > 1:
				dir = (path[1] - path[0]).normalized()
			look_at(player.global_position)
			velocity = move_and_slide(dir*100.0)
			_shoot(dir)
			return
		look_at(player.global_position)
		var dir = -(global_position - player.global_position).normalized()
		velocity = move_and_slide(dir*100.0)
		_shoot(dir)
		
func _shoot(dir):
	if get_node("FireTimer").time_left == 0:
		var p = Projectile.instance()
		p.velocity = dir*200.0
		p.set_global_position(get_node("Position2D").global_position)
		level.add_object(p)
		get_node("FireTimer").start(1.0)
		
func _physics_process(delta):
	_move_and_shoot()
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		# Null check:
		# https://www.reddit.com/r/godot/comments/i64obq
		# /proper_way_to_check_if_null/g0td8ia
		if collision.collider != null:
			if collision.collider.name == 'Player':
				# https://godotengine.org/qa/2220/
				# how-contact-position-when-collision-happens-physics-engine
				# check answer by zendorf
				var pos = collision.position
				player.take_damage(3.0, pos)

