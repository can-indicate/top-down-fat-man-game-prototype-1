extends Sprite

onready var Sound = preload("res://BloodSounds.tscn")
var level

func _ready():
	level = get_parent().get_parent()
	var sound = Sound.instance()
	sound.global_position = global_position
	level.add_sound(sound)

func _on_Timer_timeout():
	queue_free()
