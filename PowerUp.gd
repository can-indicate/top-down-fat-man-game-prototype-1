extends Sprite


func _on_Timer_timeout():
	queue_free()


func _on_Area2D_body_entered(body):
	if body.has_method("use_powerup"):
		body.use_powerup()
		queue_free()

		
func _process(delta):
	if $Timer.time_left < 3:
		modulate.a = abs(sin(2.0*3.14159*$Timer.time_left))
