extends Area2D

var level
var velocity = Vector2(0.0, 0.0)
var damage = 100
onready var Sound = preload("res://GunshotSound.tscn")
onready var Explosion = preload("res://ExplosionSprite.tscn")

func _ready():
	level = get_parent().get_parent()
	var sound = Sound.instance()
	sound.global_position = global_position
	level.add_sound(sound)


func _process(delta):
	global_position += delta*velocity
	

func _on_Area2D_body_entered(body):
	if body.has_method("take_damage"):
		body.take_damage(damage, global_position)
		# get_node("Timer").start(0.5)
		queue_free()
	elif 'Static' in str(body) or 'TileMap' in str(body):
		var explosion = Explosion.instance()
		explosion.global_position = global_position
		level.add_object(explosion)
		queue_free()



func _on_Timer_timeout():
	queue_free()

