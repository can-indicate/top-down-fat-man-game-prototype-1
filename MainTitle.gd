extends Node2D


onready var Level = preload("res://Level.tscn")
onready var Level2 = preload("res://Level2.tscn")
onready var Level3 = preload("res://Level3.tscn")
var time = 0.0
var level = 'res://Level.tscn'

func load_game():
	var load_file = File.new()
	if load_file.file_exists("res://game.save"):
		load_file.open("res://game.save", File.READ)
		var data = JSON.parse(load_file.get_line()).get_result()
		if data != null and data.has('level'):
			level = data['level']

func _unhandled_input(event):
	if event is InputEventKey and time > 0.5:
		queue_free()
		load_game()
		if level == 'res://Level.tscn':
			get_tree().get_root().add_child(Level.instance())
		elif level == 'res://Level2.tscn':
			get_tree().get_root().add_child(Level2.instance())
		elif level == 'res://Level3.tscn':
			get_tree().get_root().add_child(Level3.instance())

func _process(delta):
	time += delta;
	$Sprite.material.set_shader_param("t", time)
