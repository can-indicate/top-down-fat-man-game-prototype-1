extends Area2D


var velocity = Vector2(0.0, 0.0)
var damage = 15
onready var Sound = preload("res://GunshotSound.tscn")
onready var Explosion = preload("res://ExplosionSprite.tscn")
var level

func _ready():
	level = get_parent().get_parent()
	var sound = Sound.instance()
	sound.global_position = global_position
	level.add_sound(sound)


func _process(delta):
	global_position += delta*velocity
	


func _on_FriendlyProjectile_body_entered(body):
	var name = str(body)
	if body.has_method("enemy_take_damage"):
		body.enemy_take_damage(damage, global_position)
		# get_node("Timer").start(0.5)
		queue_free()
	elif 'Static' in str(body) or 'TileMap' in str(body):
		var explosion = Explosion.instance()
		explosion.global_position = global_position
		level.add_object(explosion)
		queue_free()
		
		


func _on_Timer_timeout():
	queue_free()
