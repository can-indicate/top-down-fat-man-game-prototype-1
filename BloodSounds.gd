extends Node2D


func _ready():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var index = rng.randi_range(0, 4)
	var sound = get_child(index)
	sound.play()


func _on_Timer_timeout():
	queue_free()
